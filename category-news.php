<div class="row">
	<div class="col-sm-9 news">
		<h2><?php echo $category->name ?></h2>
		<?php 
		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		$items_per_page = 10;
		$total_item = wp_get_cat_postcount($category->cat_ID);
		$num_page = ceil($total_item/$items_per_page);
		$posts = get_post_by_cat_id($category->cat_ID, $items_per_page, $paged); ?>
		<div class="posts">
			<?php foreach($posts as $post) { 
				setup_postdata( $post ); 
				if ( has_post_thumbnail() ) {
					$post->img = get_the_post_thumbnail();
				} 
				else {
					if($post->post_content) {
				        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches); 
				        $first_img = $matches[1][0];
				        if(empty($first_img)){ //Defines a default image
				          $first_img = get_template_directory_uri() . "/images/duan-default-image.jpg";
				        }    
			      	}
			      	else {
			      	 	$first_img = get_template_directory_uri() . "/images/duan-default-image.jpg";
			      	}
			      	$post->img =  "<img src='".$first_img . "' />";
				}
				?>
			<div class="item">
			<div class="row">
				<div class="left-item col-sm-3">
				<a href="<?php the_permalink(); ?>">
					<div class="thumbnail">
						<?php echo $post->img?>
					</div>
				</a>
				</div>
				<div class="right-item col-sm-9">
					<a class="title" href="<?php the_permalink(); ?>">
						<?php echo $post->post_title?>
					</a>
					<p class="time">Ngày đăng: <?php echo get_the_date('d/m/Y', $post->ID);?></p>
					<div class="post-content"> 
					<?php echo strip_shortcodes(wp_trim_words( get_the_content(), 50 )); ?>
					</div>
				</div>
			</div>
			</div>
			<?php }?>
		</div>
		<?php kriesi_pagination($num_page, 2); ?>
	</div>  
  	<div class="col-sm-3">
  		<?php if ( is_active_sidebar( 'post_right_1' ) ) : ?>
  			<?php dynamic_sidebar( 'post_right_1' ); ?>
  		<?php endif; ?>	
	</div>
</div>