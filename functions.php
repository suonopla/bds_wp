<?php

/* Produces a dump on the state of WordPress when a not found error occurs */
/* useful when debugging permalink issues, rewrite rule trouble, place inside functions.php */  

function re_rewrite_rules() {
    global $wp_rewrite;
    // $wp_rewrite->author_base = $author_slug;
//  print_r($wp_rewrite);
    $wp_rewrite->author_base        = 'autor';
    $wp_rewrite->search_base        = 'buscar';
    $wp_rewrite->comments_base      = 'comentarios';
    $wp_rewrite->pagination_base    = 'page';
    $wp_rewrite->flush_rules();
}
//add_action('init', 're_rewrite_rules');

add_filter('show_admin_bar', '__return_false');

add_theme_support( 'post-thumbnails' ); 

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );


function bds_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Home Sidebar Area', 'BDS' ),
    'id'            => 'home_right_1',
    'description'   => __( 'Add widgets here to appear in your sidebar.', 'bds' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
    'name'          => __( 'Post Sidebar Area', 'BDS' ),
    'id'            => 'post_right_1',
    'description'   => __( 'Add widgets here to appear in your sidebar.', 'bds' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
  
  register_sidebar( array(
    'name'          => __( 'Footer Area', 'BDS' ),
    'id'            => 'footer_1',
    'description'   => __( 'Add widgets here to appear in your sidebar.', 'bds' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Home Content Area', 'BDS' ),
    'id'            => 'home_content_1',
    'description'   => __( 'Add widgets here to appear in your sidebar.', 'bds' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}

// unregister all widgets
function unregister_default_widgets() {     
  unregister_widget('WP_Widget_Pages');     
  unregister_widget('WP_Widget_Calendar');     
  unregister_widget('WP_Widget_Archives');     
  unregister_widget('WP_Widget_Links');     
  unregister_widget('WP_Widget_Meta');     
  unregister_widget('WP_Widget_Search');     
  //unregister_widget('WP_Widget_Text');     
  unregister_widget('WP_Widget_Categories');     
  unregister_widget('WP_Widget_Recent_Posts');     
  unregister_widget('WP_Widget_Recent_Comments');     
  unregister_widget('WP_Widget_RSS');     
  unregister_widget('WP_Widget_Tag_Cloud');     
  unregister_widget('WP_Nav_Menu_Widget');     
  unregister_widget('Twenty_Eleven_Ephemera_Widget'); 
} 
add_action('widgets_init', 'unregister_default_widgets', 11);
add_action('widgets_init', 'bds_widgets_init' );
include("widget/init.php");


function bds_scripts_basic()
{
    // Register the script like this for a plugin:
    //wp_register_script( 'custom-script', plugins_url( '/js/custom-script.js', __FILE__ ) );
    // or
    // Register the script like this for a theme:
    wp_register_script( 'custom-script', get_template_directory_uri() . '/js/jquery.min.js' );
    wp_register_script( 'custom-script', get_template_directory_uri() . '/js/bootstrap.min.js' );
    //wp_register_script( 'custom-script', get_template_directory_uri() . '/js/jssor.js' );
    //wp_register_script( 'custom-script', get_template_directory_uri() . '/js/jssor.slider.mini.js' );
 
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'bds_scripts_basic' );

function page_vi_tri_output( $post )
{
 	page_common_output_text_box($post, 'vi_tri');
}
function page_gia_ca_output( $post )
{
  page_common_output_text_box($post, 'gia_ca');
}
function page_tien_ich_output( $post )
{
  page_common_output($post, 'tien_ich');
}
function page_mat_bang_output( $post )
{
  page_common_output($post, 'mat_bang');
}
function page_tien_do_output( $post )
{
  page_common_output($post, 'tien_do');
}
function page_tin_tuc_output( $post )
{
  page_common_output($post, 'tin_tuc');
}
function page_hinh_anh_output( $post )
{
  page_common_output($post, 'hinh_anh');
}
function page_video_output( $post )
{
  page_common_output($post, 'video');
}
function page_giao_dich_output( $post )
{
  page_common_output($post, 'giao_dich');
}

function page_common_output_text_box($post, $name){
  // Use nonce for verification
  wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );
  $field_value = get_post_meta( $post->ID, $name, false );
  if( $field_value ){
    ?>
    <input style="width: 100%; padding: 5px" type="text" name="<?php echo $name;?>" id="meta_box_<?php echo $name;?>" 
      value="<?php echo $field_value[0] ?>"/>
    <?php
  }
  else {
    ?>
    <input style="width: 100%; padding: 5px" type="text" name="<?php echo $name;?>" id="meta_box_<?php echo $name;?>" 
      value=""/>
    <?php
  }

}

function page_common_output($post, $name){
  // Use nonce for verification
  wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );
  $field_value = get_post_meta( $post->ID, $name, false );
  if( $field_value ){
    wp_editor( $field_value[0], $name);
  }
  else {
    wp_editor( '', $name);
  }

}
function bds_save_postdata($post_id){
	// verify if this is an auto save routine. 
  // If it is our form has not been submitted, so we dont want to do anything
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;

  // verify this came from the our screen and with proper authorization,
  // because save_post can be triggered at other times
  if ( ( isset ( $_POST['myplugin_noncename'] ) ) && ( ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename( __FILE__ ) ) ) )
      return;

  // Check permissions
  if ( ( isset ( $_POST['post_type'] ) ) && ( 'page' == $_POST['post_type'] )  ) {
    if ( ! current_user_can( 'edit_page', $post_id ) ) {
      return;
    }    
  }
  else {
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
      return;
    }
  }

  // OK, we're authenticated: we need to find and save the data
  if ( isset ( $_POST['vi_tri'] ) ) {
    update_post_meta( $post_id, 'vi_tri', $_POST['vi_tri'] );
  }
  if ( isset ( $_POST['gia_ca'] ) ) {
    update_post_meta( $post_id, 'gia_ca', $_POST['gia_ca'] );
  }
  /*
  if ( isset ( $_POST['tien_ich'] ) ) {
    update_post_meta( $post_id, 'tien_ich', $_POST['tien_ich'] );
  }
  if ( isset ( $_POST['mat_bang'] ) ) {
    update_post_meta( $post_id, 'mat_bang', $_POST['mat_bang'] );
  }
  if ( isset ( $_POST['tien_do'] ) ) {
    update_post_meta( $post_id, 'tien_do', $_POST['tien_do'] );
  }
  if ( isset ( $_POST['tin_tuc'] ) ) {
    update_post_meta( $post_id, 'tin_tuc', $_POST['tin_tuc'] );
  }
  if ( isset ( $_POST['hinh_anh'] ) ) {
    update_post_meta( $post_id, 'hinh_anh', $_POST['hinh_anh'] );
  }
  if ( isset ( $_POST['video'] ) ) {
    update_post_meta( $post_id, 'video', $_POST['video'] );
  }
  if ( isset ( $_POST['giao_dich'] ) ) {
    update_post_meta( $post_id, 'giao_dich', $_POST['giao_dich'] );
  }
  */

}
function page_meta_box()
{
    add_meta_box( 'page-vi_tri', 'Vị trí', 'page_vi_tri_output', 'page', 'normal', 'high' );
    add_meta_box( 'page-gia_ca', 'Giá cả', 'page_gia_ca_output', 'page', 'normal', 'high' );

    //add_meta_box( 'page-tien_ich', 'Tiện ích', 'page_tien_ich_output', 'page', 'normal', 'high' );
    //add_meta_box( 'page-mat_bang', 'Mặt bằng', 'page_mat_bang_output', 'page', 'normal', 'high' );
    //add_meta_box( 'page-tien_do', 'Tiến độ', 'page_tien_do_output', 'page', 'normal', 'high' );
    //add_meta_box( 'page-tin_tuc', 'Tin tức', 'page_tin_tuc_output', 'page', 'normal', 'high' );
    //add_meta_box( 'page-hinh_anh', 'Hình ảnh', 'page_hinh_anh_output', 'page', 'normal', 'high' );
    //add_meta_box( 'page-video', 'Video', 'page_video_output', 'page', 'normal', 'high' );
    //add_meta_box( 'page-giao_dich', 'Giao dịch', 'page_giao_dich_output', 'page', 'normal', 'high' );
}
/* Do something with the data entered */
add_action( 'save_post', 'bds_save_postdata' );
add_action( 'add_meta_boxes', 'page_meta_box' );
/**
 * Add new fields to wp-admin/options-general.php page
*/
function register_fields() {
  register_setting( 'general', 'copyright_description', 'esc_attr' );
  add_settings_field(
    'extra_blog_desc_id',
    '<label for="extra_blog_desc_id">' . __( 'Copyright description' , 'copyright_description' ) . '</label>',
    'fields_html',
    'general'
  );
}
/**
 * HTML for extra settings
 */
function fields_html() {
  $value = get_option( 'copyright_description', '' );
  echo '<input class="regular-text" type="text" id="extra_blog_desc_id" name="copyright_description" value="' . esc_attr( $value ) . '" />';
}
add_action( 'admin_init' , 'register_fields');

function get_detail_project(){
  global $post;
  $post->vi_tri = get_post_meta( $post->ID, 'vi_tri', true );
  $post->tien_ich = get_post_meta( $post->ID, 'tien_ich', true );
}

function get_page_id_by_slug($slug){
  $page = get_page_by_path($slug);
  if($page) return $page->ID;
  else return 0;
}

function get_all_project_by_cat($cat_id = -1 ,$limit = 9, $page = 1){
  $args = array(
    'posts_per_page'   => $limit,
    'offset'           => ($page-1)*$limit,
    'category'         => $cat_id,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'page',
    'post_status'      => 'publish',
    'suppress_filters' => true,
  );
  return get_all_project($args);
}

function get_image($page){
  ob_start();
  ob_end_clean();
  $first_img = get_template_directory_uri() . "/images/duan-default-image.jpg";

  if ( has_post_thumbnail($page->ID) ) {
      $first_img = get_the_post_thumbnail_url($page->ID);
  } 
  else if($page->post_content) {
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $page->post_content, $matches);
    if(isset($matches[1][0])){
      $first_img = $matches[1][0];
    }
  }
  return $first_img;
}
function get_all_project($args){
  
  $pages = get_posts($args);
  return $pages;
}

function get_lastest_post($limit){
  $args = array(
    'posts_per_page'   => $limit,
    'offset'           => 0,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'post',
    'post_status'      => 'publish',
    'suppress_filters' => true 
  );
  return get_posts( $args );
}

function get_lastest_post_by_cat($cat_id, $limit){
  $args = array(
    'category'         => $cat_id,
    'posts_per_page'   => $limit,
    'offset'           => 0,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'post',
    'post_status'      => 'publish',
    'suppress_filters' => true 
  );
  return get_posts( $args );
}

function get_lastest_page($limit){
  $args = array(
    'posts_per_page'   => $limit,
    'offset'           => 0,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'page',
    'post_status'      => 'publish',
    'suppress_filters' => true ,
    'parent'           => array(20, 22)
  );
  return get_pages( $args );
}

function get_post_by_cat_id($cat_id, $limit = 20, $page = 1){
  $args = array(
    'posts_per_page'   => $limit,
    'offset'           => ($page-1)*$limit,
    'category'         => $cat_id,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'post',
    'post_status'      => 'publish',
    'suppress_filters' => true 
  );
  return get_posts( $args );
}

function get_all_category_news(){
  $parent_id = get_cat_id_by_slug('tin-tuc-va-su-kien');
  $args = array('child_of' => $parent_id, 'hide_empty' => false);
  $categories = get_categories( $args );
  return  $categories;
}

function get_all_category_project(){
  $parent_id = get_cat_id_by_slug('du-an');
  $args = array('child_of' => $parent_id, 'hide_empty' => false);
  $categories = get_categories( $args );
  return  $categories;
}

function get_all_same_page($page_id){
  $parent_id = wp_get_post_parent_id($page_id);
  if($parent_id == 0){
    return array();
  }
  $args = array('child_of' => $parent_id, 'hide_empty' => false);
  $pages = get_pages($args);
  return  $pages;
}

function get_cat_id_by_slug($slug){
  $category = get_term_by( 'slug', $slug, 'category' );
  if($category)
    return $category->term_id;
  else
    return 0;
}

function get_cat_type($cat){

  if( $cat->slug == 'tin-tuc-va-su-kien' || $cat->slug == 'tin-viet-kieu' || 
      $cat->category_parent == get_cat_id_by_slug('tin-tuc-va-su-kien')){
    return 1;
  }
  else if($cat->slug == 'du-an'){
    return 2;
  }
  else if($cat->category_parent == get_cat_id_by_slug('du-an') || $cat->category_parent == 0){
    return 3;
  }
}

function get_page_type($page_id){
  $parent_id = wp_get_post_parent_id($page_id);
  $parent = get_post( $parent_id );
  
  if( $parent_id == 0 || $parent->post_name == 'gioi-thieu'){
    return 1;
  }
  else {
    return 2;
  }
}

function get_current_menu(){
  global $wp;
  $current_url = (add_query_arg(array(),$wp->request));
  $currentMenu = 0;
  switch ($current_url) {
    case '':
    $currentMenu = 1;
    break;
    case 'gioi-thieu/gioi-thieu-chung':
    $currentMenu = 2;
    break;
    case 'du-an':
    $currentMenu = 3;
    break;
    case '':
    $currentMenu = 4;
    break;
    case 'tin-tuc-va-su-kien':
    $currentMenu = 5;
    break;
    case 'tuyen-dung':
    $currentMenu = 6;
    break;
    case 'lien-he':
    $currentMenu = 7;
    break;          
    default:
    $currentMenu = 1;
    break;
  }
  return $currentMenu;
}

function du_an_thumbnail($pages){
  
  $index = 0;
  foreach ( $pages as $page ) {
    if($index%3 == 0){
      ?>
    <div class="row">
    <?php } ?>
    <div class="col-sm-6 col-md-4">
      <a href="<?php echo get_page_link( $page->ID )?>">
        <div class="thumbnail">
          <img src="<?php echo get_image($page)?>" alt="<?php echo $page->post_title?>">
        </div>
      </a>
      <div class="caption">
        <a href="<?php echo get_page_link( $page->ID )?>"><?php echo $page->post_title?></a>
      </div>
      <?php if(get_post_meta( $page->ID, 'vi_tri', true )) {?>
      <div class="info">
        <span class="location"><?php echo get_post_meta( $page->ID, 'vi_tri', true )?></span>
      </div>
      <?php } ?>
      <?php if(get_post_meta( $page->ID, 'gia_ca', true )) {?>
      <div class="info">
        <span class="price"><?php echo get_post_meta( $page->ID, 'gia_ca', true )?></span>
      </div>
      <?php } ?>
    </div>
    <?php if(($index+1)%3 == 0){ ?>
      </div>
    <?php } 
      $index++;
    }
    if( $index%3 != 0) {
    ?>
  </div>
  <?php
  }
}
function kriesi_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
      ?>
        <nav aria-label="...">
        <ul class="pagination pagination-sm">
         <?php 
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) {
          ?>
          <li class="page-item">
            <a class="page-link" href="<?php echo get_pagenum_link(1);?>" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <?php
         }
         if($paged > 1 && $showitems < $pages) {
          ?>
          <li class="page-item">
            <a class="page-link" href="<?php echo get_pagenum_link($paged - 1);?>" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <?php
         }

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
              if($paged == $i){
                ?>
                <li class="page-item">
                  <a class="page-link" href="<?php echo get_pagenum_link($i)?>">
                    <?php echo $i?>
                  </a>
                </li>
                <?php
              }
              else {
                ?>
                 <li class="page-item">
                  <a class="page-link" href="<?php echo get_pagenum_link($i)?>">
                    <?php echo $i?>
                  </a>
                </li>
                <?php
              }
             }
         }

         if ($paged < $pages && $showitems < $pages){
         ?>
         <li class="page-item">
              <a class="page-link" href="<?php echo get_pagenum_link($paged+1)?>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
         <?php
         }
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
          ?>
          <li class="page-item">
              <a class="page-link" href="<?php echo get_pagenum_link($pages)?>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          <?php
         }
         ?>
          </ul>
        </nav>
        <?php
     }
}
function wp_get_postcount($id) {
  //return count of post in category child of ID 15
  $q = new WP_Query(array(
    'nopaging' => true,
    'tax_query' => array(
      'taxonomy' => 'category',
      'field' => 'id',
      'terms' => $id,
      'include_children' => true,
  )));
  return $q->post_count;
}    
function wp_get_cat_postcount($id) {
    $cat = get_category($id);
    $count = (int) $cat->count;
    $taxonomy = 'category';
    $args = array(
      'child_of' => $id,
    );
    $tax_terms = get_terms($taxonomy,$args);
    foreach ($tax_terms as $tax_term) {
        $count +=$tax_term->count;
    }
    return $count;
}