<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage BDS
 * @since BDS 1.0
 */

get_header(); ?>
<?php 
	$category = get_category( get_query_var( 'cat' ) ); 
	$cat_type = get_cat_type($category);
	if($cat_type == 1) {
		include("category-news.php");
	}
	else if($cat_type == 2) {
		include("du-an-page.php");
	}
	else {
		include("category-page.php");
	}

?>
<?php get_footer(); ?>	