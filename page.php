<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage BDS
 * @since BDS 1.0
 */

get_header(); 
	global $post;
	$page_type = get_page_type($post->ID);

	if( $page_type == 1) {
		include("static-page.php");
	}
	else {
		include("detail-page.php");
	}
?>
<?php get_footer(); ?>	