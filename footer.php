  </div>  
</div>  
  <div class="footer">
        <div class="container">
            <div class="row">
                <?php if ( is_active_sidebar( 'footer_1' ) ) : ?>
                  <?php dynamic_sidebar( 'footer_1' ); ?>
                <?php endif; ?> 
             </div>
         </div>
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_template_directory_uri();?>/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            // auto resize img
           var max_width = $('.post-content').width();
           $(".post-content img").each(function(){
               var w = $(this).attr('width');
               if( w > max_width){
                   console.log(w);
                   var h = $(this).attr('height');
                   h = h * (max_width / w);
                   w = max_width;
                   $(this).attr('height', h);
                   $(this).attr('width', w);
               }
           });
           $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
            });
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            
            $('#back-to-top').tooltip('show');

        })
    </script>
	<?php wp_footer(); ?>
  </body>
</html>
