<div class="row">
	<div class="col-sm-9">
		<div class="panel panel-primary green">
			<div class="panel-heading green">
				<h3 class="panel-title"><?php echo $category->name ?></h3>
			</div>
			<div class="panel-body">
				<?php 
				    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				    $items_per_page = 24;
				    $num_page = ceil($category->category_count/$items_per_page);
					$pages = get_all_project_by_cat($category->cat_ID, $items_per_page, $paged);
					echo du_an_thumbnail($pages);
				?>
			<?php kriesi_pagination($num_page, 2); ?>
			</div>
		</div>	
	</div>  
  	<div class="col-sm-3">
  		<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
			<?php dynamic_sidebar( 'home_right_1' ); ?>
		<?php endif; ?>	
  	</div>
</div>