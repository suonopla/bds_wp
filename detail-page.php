<?php 
global $post;
get_detail_project();
?>
<div class="row">
	<div class="col-sm-9 page">
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
		    <?php if(function_exists('bcn_display'))
		    {
		        bcn_display();
		    }?>
		</div>
		<?php include("inc/share-box.php") ?>
		<div class="post">
		 	<p class="title"><?php echo $post->post_title?></p>
		 	<p class="time">Ngày đăng : <?php the_date('d/m/Y');?></p>
		 	<p class="view">Số lượt đọc: <?php if(function_exists('the_views')) { the_views(); } ?></p>
		 	<div class="post-content">
		  		<?php echo wpautop($post->post_content)?>
		  	</div>
		 </div>
		<?php include("inc/contact-form.php")?>
		<?php include("inc/other-page.php") ?>
	</div>
	<div class="col-sm-3">
		<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
  			<?php dynamic_sidebar( 'home_right_1' ); ?>
  		<?php endif; ?>	
	</div>
</div>