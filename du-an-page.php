<div class="row">
	<div class="col-sm-9">
		<?php if ( is_active_sidebar( 'home_content_1' ) ) : ?>
  			<?php dynamic_sidebar( 'home_content_1' ); ?>
  		<?php endif; ?>	
	</div>  
  	<div class="col-sm-3">
  		<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
  			<?php dynamic_sidebar( 'home_right_1' ); ?>
  		<?php endif; ?>	
  	</div>
</div>