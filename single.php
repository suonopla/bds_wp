<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage BDS
 * @since BDS 1.0
 */

get_header(); 
global $post;
?>
	<div class="row">
		<div class="col-md-9 col-sm-12 post">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
			    <?php if(function_exists('bcn_display'))
			    {
			        bcn_display();
			    }?>
			</div>
			<?php include("inc/share-box.php") ?>
			 <div class="post">
			 	<p class="title"><?php echo $post->post_title?></p>
			 	<p class="time">Ngày đăng : <?php the_date('d/m/Y');?></p>
			 	<p class="view">Số lượt đọc: <?php if(function_exists('the_views')) { the_views(); } ?></p>
			 	<div class="post-content">
			  		<?php echo wpautop($post->post_content)?>
			  	</div>
			 </div>
			 <?php include("inc/other-post.php") ?>
		</div>
		<div class="col-md-3 hidden-sm">
			<?php if ( is_active_sidebar( 'post_right_1' ) ) : ?>
  				<?php dynamic_sidebar( 'post_right_1' ); ?>
  			<?php endif; ?>	
		</div>
	</div>
<?php get_footer(); ?>	