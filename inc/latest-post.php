<?php
$cat_id = get_cat_id_by_slug('tin-tuc-va-su-kien');
$posts = get_lastest_post_by_cat($cat_id, 5);
?>
<div class="list-group latest-post">
	<a class="view-all" href="<?php echo get_category_link($cat_id)?>">Xem hết</a>
	<p class="list-group-item active green">Tin tức và sự kiện</p>
	<?php foreach($posts as $post) { setup_postdata( $post ); ?>
	<div class="list-group-item">
		<a href="<?php the_permalink(); ?>"><?php echo $post->post_title?></a>
		<span class="time">(<?php echo get_the_date('d/m/Y', $post->ID);?>)</span>
	</div>
	<?php }
	wp_reset_postdata();?>
</div>