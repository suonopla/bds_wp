<?php 
global $post;
$pages = get_all_same_page($post->ID);
?>
<div class="list-group list-news-cat">
	<?php 
	foreach($pages as $page) { ?>
	<div class="list-group-item">
		<a href="<?php echo get_page_link( $page->ID ) ?>"><?php echo $page->post_title ?></a>
	</div>
	<?php } ?>
</div>