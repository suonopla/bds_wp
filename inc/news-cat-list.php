<?php 
$categories = get_all_category_news();
?>
<div class="list-group list-news-cat">
	<?php 
	foreach($categories as $category) { ?>
	<div class="list-group-item">
		<a href="<?php echo get_category_link( $category->term_id ) ?>"><?php echo $category->name ?></a>
	</div>
	<?php } ?>
</div>