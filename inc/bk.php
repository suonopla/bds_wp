<div class="row">
			<div class="col-sm-6 col-md-4">
				<?php $category = get_term_by( 'slug', 'du-an-dau-tu', 'category' );?>
				<a href="<?php echo get_category_link($category->term_id)?>">
					<div class="thumbnail thumbnail-abc">
						<img src="<?php echo get_template_directory_uri();?>/images/bds_dacbiet.jpg">
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4">
				<?php $category = get_term_by( 'slug', 'du-an-khuyen-mai', 'category' );?>
				<a href="<?php echo get_category_link($category->term_id)?>">
					<div class="thumbnail thumbnail-abc">
						<img src="<?php echo get_template_directory_uri();?>/images/bds_giatot.jpg">
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4">
				<?php $category = get_term_by( 'slug', 'du-an-moi', 'category' );?>
				<a href="<?php echo get_category_link($category->term_id)?>">
					<div class="thumbnail thumbnail-abc">
						<img src="<?php echo get_template_directory_uri();?>/images/bds_moi.jpg">
					</div>
				</a>
			</div>
		</div>