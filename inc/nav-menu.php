<nav class="navbar navbar-inverse" role="navigation" id="slide-nav">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
    </div>
    <div class="navbar-collapse collapse" id="menu">
      <ul class="nav navbar-nav navbar-left">
      <?php 
      $menus = wp_get_nav_menu_items('Top menu');
      $this_item = current( wp_filter_object_list( $menus, array( 'object_id' => get_queried_object_id() ) ) );
      //var_dump($this_item->ID);
      foreach ($menus as $key => $menu) {
        ?>
        <li <?php if($this_item == $menu) echo "class='current'"?>><a href="<?php echo $menu->url?>">
          <?php echo $menu->title?></a></li>
        <?php
      }
      ?>
      </ul>
    </div>
</nav>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.lavalamp.min.js"></script>
<script type="text/javascript">
  $('.navbar-nav').lavalamp({
    easing: 'easeOutBack',
    activeObj: '.current'
  });
</script>