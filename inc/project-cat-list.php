<?php 
$categories = get_all_category_project();
?>
<div class="list-group list-projects-cat">
	<?php 
	foreach($categories as $category) { ?>
	<div class="list-group-item">
		<span class="glyphicon glyphicon-home"></span>
		<a href="<?php echo get_category_link( $category->term_id ) ?>"><?php echo $category->name ?></a>
	</div>
	<?php } ?>
</div>