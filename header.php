<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage BDS
 * @since BDS 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	 <!-- Bootstrap -->
    <!-- <link href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri();?>/css/main.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri();?>/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div class="header">
		<div class="container">
			<div class="menu row" id="primary-nav">
				<div class="col-sm-12 mt-40 hidden-xs"></div>
				<div class="col-sm-3 hidden-xs">
					<img src="<?php echo get_template_directory_uri();?>/images/logo_HTL2.png"> 
				</div>
				<div class="col-sm-9 col-xs-12">
					<?php include("inc/nav-menu.php"); ?>
				</div>
				<div class="col-sm-12 mt-20 hidden-xs"></div>
			</div>
		</div>	
	</div>
	<div class="slider">
		<div class="container">
			<?php include("inc/slider.php") ?>
		</div>
	</div>
	<div class="main">
		<div class="container">
		