<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage BDS
 * @since BDS 1.0
 */

get_header(); 
global $post;
?>
		<div class="row">
			<div class="col-sm-9 col-xs-12 static-page">
				<h2><?php echo $post->post_title ?></h2>
				 <div class="post-content">
				  	<?php echo $post->post_content?>
				 </div>
			</div>
			<div class="col-sm-3 col-xs-12">
				<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
  					<?php dynamic_sidebar( 'home_right_1' ); ?>
  				<?php endif; ?>	
			</div>
		</div>
	</div>
<?php get_footer(); ?>	