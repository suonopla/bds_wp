<?php global $post; ?>
<div class="row">
	<div class="col-sm-9">
		<div class="panel panel-primary green">
			<div class="panel-heading green">
				<h3 class="panel-title"><?php echo $post->post_title ?></h3>
			</div>
			<div class="panel-body">
				<?php 
					$pages = get_all_project_by_parent_id($post->ID);
					echo du_an_thumbnail($pages);
				?>
			</div>
		</div>
	</div>  
  	<div class="col-sm-3">
  		<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
  			<?php dynamic_sidebar( 'home_right_1' ); ?>
  		<?php endif; ?>	
  	</div>
</div>