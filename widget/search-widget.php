<?php 
class My_Widget_6 extends WP_Widget {

	function __construct() {
        $widget_ops = array( 
        	'classname' => 'example', 
        	'description' => __('Search box', 'example') 
        );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'widget-6' );
        parent::__construct( 'widget-6', __('Search box', 'example'), $widget_ops, $control_ops );
    }
    function form( $instance ) {
	}

    function update( $new_instance, $old_instance ){
	}

	function widget( $args, $instance ){
		?>
		<aside class="widget widget-search-box">
			<div id="custom-search-input">
			    <div class="input-group col-md-12">
			        <input type="text" class="  search-query form-control" placeholder="Search" />
			        <span class="input-group-btn">
			            <button class="btn btn-success" type="button">
			                <span class=" glyphicon glyphicon-search"></span>
			            </button>
			        </span>
			    </div>
			</div>
		</aside>
		<?php
    }
}
?>