<?php 
class My_Widget_3 extends WP_Widget {

	function __construct() {
        $widget_ops = array( 
        	'classname' => 'example', 
        	'description' => __('A widget that displays facebook box ', 'example') 
        );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'widget-3' );
        parent::__construct( 'widget-3', __('Facebook', 'example'), $widget_ops, $control_ops );
    }
    function form( $instance ) {
	}

    function update( $new_instance, $old_instance ) {
	}

	function widget( $args, $instance ){
		?>
		<aside class="widget">
			<?php echo do_shortcode('[custom-facebook-feed]'); ?>
		</aside>
		<?php
	}
}
?>