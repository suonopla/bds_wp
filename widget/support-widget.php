<?php 
class My_Widget_4 extends WP_Widget {

	function __construct() {
        $widget_ops = array( 
        	'classname' => 'example', 
        	'description' => __('A widget that displays the latest post ', 'example') 
        );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'widget-4' );
        parent::__construct( 'widget-4', __('Hỗ trợ trực tuyến', 'example'), $widget_ops, $control_ops );
    }
    function form( $instance ) {
	}

    function update( $new_instance, $old_instance ) {
	}

	function widget( $args, $instance ){
		?>
		<aside class="widget support-widget">
			<div class="title_support">
				<span class="glyphicon glyphicon-headphones"></span>
				<span class="title">Hỗ trợ trực tuyến</span>
			</div>
			<img style="margin:10px 0 0 5px;" src="<?php echo get_template_directory_uri()?>/images/hotline12-4.gif" width="256" height="152">
		</aside>
		<?php
    }
}
?>