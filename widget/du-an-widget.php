<?php 
class My_Widget_7 extends WP_Widget {

	function __construct() {
        $widget_ops = array( 
        	'classname' => 'example', 
        	'description' => __('A widget that displays the latest post ', 'example') 
        );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'widget-7' );
        parent::__construct( 'widget-7', __('List page box', 'example'), $widget_ops, $control_ops );
    }
    function form( $instance ) {
	    $cats = get_categories();
	    $cat_id = 0;
	    if( !empty( $instance['cat_id'] ) ) {
	        $cat_id = $instance['cat_id'];
	    }
	    if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		if ( isset( $instance[ 'num' ] ) ) {
			$num = $instance[ 'num' ];
		}
		else {
			$num = 12;
		}

	    ?>
	    <p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

	    <p>
	        <label for="<?php echo $this->get_field_name( 'text' ); ?>"><?php _e( 'Category:' ); ?></label>
	        <select class="widefat" id="<?php echo $this->get_field_id( 'cat_id' ); ?>" name="<?php echo $this->get_field_name( 'cat_id' ); ?>">
	        <?php foreach ($cats as $key => $cat) {
	        	if($cat->term_id == $cat_id) { ?>
	        		<option selected value="<?php echo $cat->term_id?>"><?php echo $cat->name?></option>
	        	<?php
	        	}
	        	else {
	        	?>
	        		<option value="<?php echo $cat->term_id?>"><?php echo $cat->name?></option>
	        	<?php
	        	}
	        }?>
	        </select>
	    </p>
	    <p>
		<label for="<?php echo $this->get_field_id( 'num' ); ?>"><?php _e( 'Num:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'num' ); ?>" name="<?php echo $this->get_field_name( 'num' ); ?>" type="text" value="<?php echo esc_attr( $num ); ?>" />
		</p>

	    <div class='mfc-text'></div>
	 
	    <?php
	}

    function update( $new_instance, $old_instance ) {
	    $instance = $old_instance;
	 
	    //Strip tags from title and name to remove HTML
	    $instance['title'] = strip_tags( $new_instance['title'] );
	    $instance['cat_id'] = strip_tags( $new_instance['cat_id'] );
	    $instance['num'] = strip_tags( $new_instance['num'] );
	 
	    return $instance;
	}

	function widget( $args, $instance ){
		$cat_id = $instance['cat_id'];
		$num = isset($instance['num']) ? $instance['num'] : 12;
		?>
		<div class="panel panel-primary green">
			<a class="view-all" href="<?php echo get_category_link($cat_id)?>">Xem hết</a>
			<div class="panel-heading green">
				<h3 class="panel-title"><?php echo $instance['title']?></h3>
			</div>
			<div class="panel-body">
				<?php 
					$pages = get_all_project_by_cat($cat_id, $num);
					echo du_an_thumbnail($pages);
				?>
			</div>
		</div>
		<?php
    }
}
?>