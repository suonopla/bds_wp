<?php 
class My_Widget_2 extends WP_Widget {

	function __construct() {
        $widget_ops = array( 
        	'classname' => 'example', 
        	'description' => __('A widget that displays the latest post ', 'example') 
        );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'widget-2' );
        parent::__construct( 'widget-2', __('Dự án Categories', 'example'), $widget_ops, $control_ops );
    }
    function form( $instance ) {

    	$cats = get_categories();
	    $cat_id = 0;
	    if( !empty( $instance['cat_id'] ) ) {
	        $cat_id = $instance['cat_id'];
	    }
	    if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
	    ?>
	    <p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

	    <p>
	        <label for="<?php echo $this->get_field_name( 'text' ); ?>"><?php _e( 'Category:' ); ?></label>
	        <select class="widefat" id="<?php echo $this->get_field_id( 'cat_id' ); ?>" name="<?php echo $this->get_field_name( 'cat_id' ); ?>">
	        <?php foreach ($cats as $key => $cat) {
	        	if($cat->term_id == $cat_id) { ?>
	        		<option selected value="<?php echo $cat->term_id?>"><?php echo $cat->name?></option>
	        	<?php
	        	}
	        	else {
	        	?>
	        		<option value="<?php echo $cat->term_id?>"><?php echo $cat->name?></option>
	        	<?php
	        	}
	        }?>
	        </select>
	    </p>
	    <div class='mfc-text'></div>
	    <?php
	}

    function update( $new_instance, $old_instance ) {
	    $instance = $old_instance;
	 
	    //Strip tags from title and name to remove HTML
	    $instance['title'] = strip_tags( $new_instance['title'] );
	    $instance['cat_id'] = strip_tags( $new_instance['cat_id'] );
	 
	    return $instance;
	}

	function widget( $args, $instance ){
		$parent_id = isset($instance['cat_id']) ? $instance['cat_id'] : 0;
		$args = array('child_of' => $parent_id, 'hide_empty' => false);
  		$categories = get_categories( $args );
		?>
		<aside class="widget list-group list-projects-cat">
			<?php 
			foreach($categories as $category) { ?>
			<div class="list-group-item">
				<span class="glyphicon glyphicon-home"></span>
				<a href="<?php echo get_category_link( $category->term_id ) ?>"><?php echo $category->name ?></a>
			</div>
			<?php } ?>
		</aside>
		<?php
	}
}
?>