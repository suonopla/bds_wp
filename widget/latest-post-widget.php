<?php 
class My_Widget extends WP_Widget {

	function __construct() {
        $widget_ops = array( 
        	'classname' => 'example', 
        	'description' => __('A widget that displays the latest post ', 'example') 
        );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'widget-1' );
        parent::__construct( 'widget-1', __('Category', 'example'), $widget_ops, $control_ops );
    }
    function form( $instance ) {
	    $cats = get_categories();
	    $cat_id = 0;
	    if( !empty( $instance['cat_id'] ) ) {
	        $cat_id = $instance['cat_id'];
	    }
	    if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		if ( isset( $instance[ 'num' ] ) ) {
			$num = $instance[ 'num' ];
		}
		else {
			$num = 5;
		}

	    ?>
	    <p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

	    <p>
	        <label for="<?php echo $this->get_field_name( 'text' ); ?>"><?php _e( 'Category:' ); ?></label>
	        <select class="widefat" id="<?php echo $this->get_field_id( 'cat_id' ); ?>" name="<?php echo $this->get_field_name( 'cat_id' ); ?>">
	        <?php foreach ($cats as $key => $cat) {
	        	if($cat->term_id == $cat_id) { ?>
	        		<option selected value="<?php echo $cat->term_id?>"><?php echo $cat->name?></option>
	        	<?php
	        	}
	        	else {
	        	?>
	        		<option value="<?php echo $cat->term_id?>"><?php echo $cat->name?></option>
	        	<?php
	        	}
	        }?>
	        </select>
	    </p>
	    <p>
		<label for="<?php echo $this->get_field_id( 'num' ); ?>"><?php _e( 'Num:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'num' ); ?>" name="<?php echo $this->get_field_name( 'num' ); ?>" type="text" value="<?php echo esc_attr( $num ); ?>" />
		</p>

	    <div class='mfc-text'></div>
	 
	    <?php
	}

    function update( $new_instance, $old_instance ) {
	    $instance = $old_instance;
	 
	    //Strip tags from title and name to remove HTML
	    $instance['title'] = strip_tags( $new_instance['title'] );
	    $instance['cat_id'] = strip_tags( $new_instance['cat_id'] );
	    $instance['num'] = strip_tags( $new_instance['num'] );
	 
	    return $instance;
	}

	function widget( $args, $instance ){
		$cat_id = $instance['cat_id'];
		$num = isset($instance['num']) ? $instance['num'] : 5;
		$posts = get_lastest_post_by_cat($cat_id, $num);
		?>
		<aside class="widget list-group latest-post">
			<a class="view-all" href="<?php echo get_category_link($cat_id)?>">Xem hết</a>
			<p class="list-group-item active green"><?php echo $instance['title'] ?></p>
			<?php foreach($posts as $post) { setup_postdata( $post ); ?>
			<div class="list-group-item">
				<a href="<?php the_permalink(); ?>"><?php echo $post->post_title?></a>
				<span class="time">(<?php echo get_the_date('d/m/Y', $post->ID);?>)</span>
			</div>
			<?php }
			wp_reset_postdata();?>
		</aside>
		<?php
    }
}
?>